local helpers = require('helpers')
local gridHelper = require('grid')

local fluid = {}
fluid.__index = fluid

function fluid.new(vel, density, viscosity, divergence, pressure)
    return setmetatable({vel = vel, density = density, vis = viscosity,
                         div = divergence, pres = pressure}, fluid)
end

function fluid:advect(grid, coord, gridScale, dt, advected)
    local scale = 1 / gridScale
    local x, y = unpack(coord)
    local newPosX = x - dt * scale * grid[gridHelper.t2o(x, y)].velocity[1]
    local newPosY = y - dt * scale * grid[gridHelper.t2o(x, y)].velocity[2]
    if advected == "velocity" then
        return helpers:bilerpv2(x - 1, y - 1, x + 1, y + 1, newPosX, newPosY,
                            grid[gridHelper.t2o(x - 1, y)].velocity, grid[gridHelper.t2o(x, y + 1)].velocity, 
                            grid[gridHelper.t2o(x + 1, y)].velocity, grid[gridHelper.t2o(x, y - 1)].velocity)
    else
        return helpers:bilerp(x - 1, y - 1, x + 1, y + 1, newPosX, newPosY,
                            grid[gridHelper.t2o(x - 1, y)].density, grid[gridHelper.t2o(x, y + 1)].density, 
                            grid[gridHelper.t2o(x + 1, y)].density, grid[gridHelper.t2o(x, y - 1)].density)
    end
end

function fluid:diffuse(grid, coord, alpha, beta)
    fluid:jacobiViscosity(grid, coord, alpha, beta)
end

function fluid:jacobiPressure(grid, coord, alpha, beta) -- Ax = b [du / dt = v * nabla^2 u in implicit form]
    local x, y = coord[1], coord[2]
    local l, r, t, b = grid[gridHelper.t2o(x - 1, y)].pressure, grid[gridHelper.t2o(x + 1, y)].pressure,
                       grid[gridHelper.t2o(x, y + 1)].pressure, grid[gridHelper.t2o(x, y - 1)].pressure
    local bc = grid[gridHelper.t2o(x, y)].pressure

    return (l + r + t + b + alpha * bc) / beta
end

function fluid:jacobiViscosity(grid, coord, alpha, beta)
    local x, y = coord[1], coord[2]
    local lx, rx, tx, bx = grid[gridHelper.t2o(x - 1, y)].velocity[1], grid[gridHelper.t2o(x + 1, y)].velocity[1],
                           grid[gridHelper.t2o(x, y + 1)].velocity[1], grid[gridHelper.t2o(x, y - 1)].velocity[1]
    local ly, ry, ty, by = grid[gridHelper.t2o(x - 1, y)].velocity[2], grid[gridHelper.t2o(x + 1, y)].velocity[2],
                           grid[gridHelper.t2o(x, y + 1)].velocity[2], grid[gridHelper.t2o(x, y - 1)].velocity[2]
    local bcx, bcy = b[gridHelper.t2o(x, y)][1], b[gridHelper.t2o(x, y)][2]
    return {
        (lx + rx + tx + bx + alpha * bcx) / beta,
        (ly + ry + ty + by + alpha * bcy) / beta,
    }
end

function fluid:divergence(grid, coord, gridScale)
    local x, y = coord[1], coord[2]
    local l, r, t, b = grid[gridHelper.t2o(x - 1, y)].velocity[1], grid[gridHelper.t2o(x + 1, y)].velocity[1],
                       grid[gridHelper.t2o(x, y + 1)].velocity[2], grid[gridHelper.t2o(x, y - 1)].velocity[2]
    return gridScale * ((r - l) + (t - b))
end

function fluid:gradientSubtract(grid, coord, gridScale)
    local x, y = coord[1], coord[2]
    local l, r, t, b = grid[gridHelper.t2o(x - 1, y)].pressure, grid[gridHelper.t2o(x + 1, y)].pressure,
                       grid[gridHelper.t2o(x, y + 1)].pressure, grid[gridHelper.t2o(x, y - 1)].pressure
    grid[gridHelper.t2o(x, y)].velocity[1] = grid[gridHelper.t2o(x, y)].velocity[1] - (r - l);
    grid[gridHelper.t2o(x, y)].velocity[2] = grid[gridHelper.t2o(x, y)].velocity[2] - (t - b);
end

return fluid