local helpers = {}
helpers.__index = helpers

function helpers:lerp(x1, x2, x3, q1, q2)
    local t = (x3 - x1) / (x2 - x1)
    return q1 * (1 - t) + q2 * t
end

function helpers:bilerp(ax, ay, -- Value of lowermost coordinates
                        bx, by,  -- Uppermost coordinates value
                        px, py, -- Value to be interpolated
                        q1, q2, q3, q4 -- Value of functions (ll, lr, ul, ur)
                       )
    local ll, lr, ul, ur = {ax, ay}, {bx, ay}, {ax, by}, {bx, by}
    local r1 = helpers:lerp(ll[1], lr[1], px, q1, q2)
    local r2 = helpers:lerp(ul[1], ur[1], px, q3, q4)
    return helpers:lerp(ay, by, py, r1, r2)
end

function helpers:bilerpv2(ax, ay, -- Value of lowermost coordinates
                        bx, by,  -- Uppermost coordinates value
                        px, py, -- Value to be interpolated
                        q1, q2, q3, q4
                       )
    return {
        helpers:bilerp(ax, ay, bx, by, px, py, q1[1], q2[1], q3[1], q4[1]),
        helpers:bilerp(ax, ay, bx, by, px, py, q1[2], q2[2], q3[2], q4[2])
    }
end

return helpers