local fluid = require('fluid')
local gridHelper = require('grid')

local grid = {}

local n = 400

function love.load()
    love.window.setMode(600, 600)

    for i = 0, 200 * 200, 1 do
        grid[i] = {}
        grid[i].velocity = {math.random(0, 1) ,math.random(0, 1)}
        grid[i].pressure = 0
        grid[i].density = math.random(0, 1)
    end
end

function love.update(dt)
    for i = 1, 198 do
        for j = 1, 198 do
            local t = {i, j}
            fluid:advect(grid, t, 1, dt, "velocity")
            --fluid:diffuse(grid, {i, j}, -1, 4)
        end
    end
end

function love.draw() 
    for i = 0, 199 do
        for j = 0, 199 do
            local currentGrid = grid[gridHelper.t2o(i, j)]
            love.graphics.setColor(currentGrid.density, currentGrid.density, currentGrid.density)
            love.graphics.rectangle('fill', i , j , 1, 1)
        end
    end
end