local grid = {}
grid.__index = grid

function grid.new(x, y, size)
    local arr = {}
    return setmetatable(
        { arr = arr, x = x, y = y, n = size }, grid
    )
end

function grid.t2o(xn, yn)
    return yn * 200 + xn
end

function grid:update(dt)

end

function grid:render()

end

return grid